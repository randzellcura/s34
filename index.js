const { response } = require('express')
const express = require('express')
const app = express()
const port = 3000
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.listen(port, () => console.log(`Server is running at localhost:${port}`))

let users = [
    {
        "username": "Ivan",
        "password": "submarine123"
    },
    {
        "username": "Randzel",
        "passwprd": "passwprd"
    }
]
// 1 & 2. Create a GET route that will access the /home route that will print out
// a simple message.
app.get("/home", (request, response) => {
    response.send(`Welcome to the home page`)
})

// 3 & 4. Create a GET route that will access the /users route that will retrieve
// all the users in the mock database.
app.post("/users", (request, response) => {
    response.send(`${request.body.username} ${request.body.password}`)
})
// 5. Create a DELETE route that will access the /delete-user route to
// remove a user from the mock database.
app.delete("/delete-user", (request, response) => {
    for (let i = 0; i < users.lenght; i++) {
        if (request.body.username == users[i].username) {
            users.splice(i, 1)
            message = `User has been deleted!`
            break
        } else {
            message = `User does not exist!`
        }
    }
    response.send(message)
})